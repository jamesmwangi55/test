var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var random = require('./data/random.js');


// configure app to user bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('public'));

var port = process.env.PORT || 80; // set port

// routes for api

var router = express.Router(); // get an instance of the express router

// middleware to use for all requests
router.use(function(req, res, next){
    // do logging
    console.log('Something is happening.');
    next();
});

// test route to make sure everything is working(accessed at GET http://localhost:3000/api)

router.get('/', function (req, res) {
    res.json({message: 'hooray! welcome to our api!'});
});

// on routes that end with random

router.route('/random')

    // create a bear
    .post(function(req, res){
        var newRandom = {};

        var value = req.body.value;
        var date = req.body.date;

        // save the random value and check for errors
        random.create(value, date,  function (err, resNewDoc) {
            if(err)
            res.send(err);

            res.json(resNewDoc);
        });

    })

    // get all the random values
    .get(function (req, res) {
        random.list(function(err, randomValues){
            if(err)
                res.send(err);

            res.json(randomValues);
        })
    });




// register routes
app.use('/api', router);

// application -------------------------------------------------------------
app.get('*', function(req, res) {
    res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

//start the server
app.listen(port);
console.log('Magic happens on port: ' + port);

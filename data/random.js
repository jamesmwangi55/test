var path = require('path');
var Datastore = require('nedb');

var db = {
    randomValue: new Datastore({filename: path.join(__dirname, 'randomValue.db'), autoload: true})
};

var randomValue   = {
    create: function(value, date, callback){
        db.randomValue.insert({value: value, date: date}, callback);
    },
    list: function(callback){
        db.randomValue.find({}).exec(callback);
    },
    remove: function(id, callback){
        db.randomValue.remove({_id: id}, callback);
    }
};

module.exports = randomValue;
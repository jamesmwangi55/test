var testApp = angular.module('testApp', [])
    .controller('mainController', function ($scope, $http){
        // when landing on the page, get all the random values and show them

        $scope.name = "James Mwangi"

        $http.get('/api/random')
            .success(function(data){
                $scope.data = data;
                console.log(data);
            })
            .error(function(data){
                console.log('Error: ' + data);
            });

        $scope.createValues = function()
        {
            console.log('clicked');

            $scope.randomData = {};

            $scope.randomData.date = new Date();
            $scope.randomData.value = (function generateUUID(){
                var d = new Date().getTime();
                var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                    var r = (d + Math.random()*16)%16 | 0;
                    d = Math.floor(d/16);
                    return (c=='x' ? r : (r&0x3|0x8)).toString(16);
                });
                return uuid;
               // console.log(uuid);
            })();



            // create random value here

            $http.post('/api/random', $scope.randomData)
                .success(function(data) {
                    $scope.successData = data;
                    console.log(data);
                });
        }
    });


//
//function mainController($scope, $http){
//
//}
